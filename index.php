<?php 

require_once ('animal.php');
require_once ('frog.php');
require_once ('ape.php');

$sheep = new Animal("shaun");

echo $sheep->name; // "shaun"
echo "<br>";
echo $sheep->legs; // 2
echo "<br>";
echo $sheep->cold_blooded; // false

$sungokong = new Ape("kera sakti");
$sungokong->yell(); // "Auooo"

/* Test Code */
// echo '<br>';
// echo $sungokong->name;

$kodok = new Frog("buduk");
$kodok->jump();

/* Test Code */
// echo $kodok->name; // "hop hop"
// echo $kodok->legs; // "hop hop"


?>